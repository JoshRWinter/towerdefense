#include <windows.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <stdio.h>
#include "comgl.h"
#include "defs.h"
const char *desc(int);
extern const char *howtoplay,*about,*endgame;
extern int main_line,wndproc_line,core_line,object_line,misc_line;
int menu_line;
const char *r="reddit: geronimo25";
void mainmenu(struct global *global,HDC hdc,long long *timestep){
	struct generic menu=global->background;
	menu.texture=TID_MENU;
	FONTPROFILE font1={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->bauhaus,0.43f},
	font2={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->bauhaus,0.2f};
	struct{float x,y;const char *name;}item[]={
		{-14.0f,-1.0f,"-PLAY-"},
		{11.5f,-0.5f,"-CONTINUE-"},
		{-14.0f,0.0f,"-SETTINGS-"},
		{11.5f,0.5f,"-HOW TO PLAY-"},
		{-14.0f,1.0f,"-ABOUT-"},
		{12.0f,1.5f,"-QUIT-"}
	};
	float intensity=0.0f;
	while(global->running){
		int play=false;
		const char *message=NULL;
		long long start=counter();
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_MENU].object);
		draw(global,&menu);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT2].object);
		int hover=false;
		for(int i=0;i<6;i++){
			float len=textlen(&font1,item[i].name),x=-len/2.0f;
			targetf(&item[i].x,0.3f,x);
			if(global->crosshair.x>item[i].x&&global->crosshair.y>item[i].y&&
			global->crosshair.x<item[i].x+len&&global->crosshair.y<item[i].y+font1.fontsize){
				hover=true;
				if(global->click){
					global->click=false;
					if(i==0)play=true;
					else if(i==1){
						reset(global);
						if(!loadgame(global))message="You currently have no active games.";
						else{
							global->pilot.loc=-1;
							global->waypoint(&global->pilot);
							return;
						}
					}
					else if(i==2)settings(global,hdc,timestep);
					else if(i==3)message=howtoplay;
					else if(i==4)message=about;
					else if(i==5)PostMessage(global->hwnd,WM_CLOSE,1,0);
				}
				intensity+=0.1f;
				glUniform3f(global->uniform.rgb,1.0f,1.0f-intensity,1.0f-intensity);
			}
			drawtext(&font1,item[i].x,item[i].y,item[i].name);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		}
		if(!hover)intensity=0.0f;
		char lines[35];
		sprintf(lines,"%d lines of code!",main_line+wndproc_line+core_line+object_line+menu_line+misc_line+18+36);
		drawtext(&font2,global->rect.left+0.1f,global->rect.bottom-font2.fontsize,lines);
		if(play&&selectlevel(global,hdc,timestep)){
			reset(global);
			return;
		}
		if(message!=NULL)popup(global,hdc,timestep,message,0);
		if(global->key_esc)PostMessage(global->hwnd,WM_CLOSE,1,0);
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
}
void buildmenu(struct global *global,HDC hdc,long long *timestep){
	unsigned texture=screenshot(global->device.right,global->device.bottom,1);
	glUniform1f(global->uniform.rot,0.0f);
	struct generic buildmenu={TID_BUILDMENU,-1.65f,global->rect.top*1.3f,
	TOWER_SIZE*6.0f,1.0f,0.0f},
	descbox={TID_BUILDMENU,-3.25f,global->rect.top+1.3f,6.5f,3.5f,0.0f};
	struct generic turret={TID_LASERTOWER,buildmenu.x+0.07f,buildmenu.y+0.4f,TOWER_SIZE,TOWER_SIZE,-0.035f};
	FONTPROFILE menu1={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.3f},
	descfp={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.25f};
	int hold;
	while(global->running){
		long long start=counter();
		
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0f);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
		targetf(&buildmenu.y,0.4f,global->rect.top);
		targetf(&turret.y,0.4f,buildmenu.y+0.4f);
		draw(global,&buildmenu);
		
		float xoffset=0.0f;
		struct generic button=turret;
		int hover=false;
		for(int i=0;i<5;i++){
			if(mouseover(global,&button)){
				hover=true;
				hold++;
				if(global->click){
					global->click=false;
					global->turretplace.texture=button.texture;
					global->turretplace.x=global->crosshair.x-(TOWER_SIZE/2.0f);
					global->turretplace.y=global->crosshair.y-(TOWER_SIZE/2.0f);
					goto BUILDMENU_END;
				}
				struct stats stats;
				getstats(texturetype(button.texture),&stats);
				glUniform1f(global->uniform.rot,0.0f);
				if(hold>30){
					glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
					draw(global,&descbox);
					glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
					drawtext(&descfp,descbox.x+0.16f,descbox.y+0.2f,desc(i));
					char *statstext=malloc(65);
					drawtext(&descfp,descbox.x+0.2f,descbox.y+2.25f,"PRICE:\nDAMAGE:\nFIRE RATE:\nRANGE:");
					sprintf(statstext,"$%d\n%d\n%.1f/second\n%.1f",stats.price,stats.damage,60.0f/stats.firerate,stats.range);
					drawtext(&descfp,descbox.x+2.0f,descbox.y+2.25f,statstext);
					free(statstext);
				}
				glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
				drawtext(&menu1,buildmenu.x+0.05f,buildmenu.y+0.05f,stats.name);
				glUniform3f(global->uniform.rgb,1.4f,1.4f,1.4f);
			}
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[button.texture].object);
			draw(global,&button);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
			button.texture++;
			button.x+=TOWER_SIZE*1.19f;
		}
		glUniform1f(global->uniform.rot,0.0f);
		if(!hover)hold=0;
		
		if(global->key_esc){
			global->key_esc=false;
			break;
		}
		if(global->crosshair.x<buildmenu.x||global->crosshair.x>buildmenu.x+buildmenu.w||
		global->crosshair.y>global->rect.top+buildmenu.h)break;
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	BUILDMENU_END:
	glDeleteTextures(1,&texture);
}
void pausemenu(struct global *global,HDC hdc,long long *timestep){
	unsigned texture=screenshot(global->device.right,global->device.bottom,5);
	glUniform1f(global->uniform.rot,0.0f);
	FONTPROFILE font={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->bauhaus,0.4f},
	font2={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->bauhaus,0.3f};
	struct generic box={TID_BUILDMENU,-2.0f,-1.5f,4.0f,3.0f,0.0f};
	struct{int show;const char *m;}message={false,"Are you sure you want to quit?\nYour progress will be saved."};
	while(global->running){
		long long start=counter();
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
		draw(global,&box);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT2].object);
		float len=textlen(&font,"[ PAUSED ]");
		drawtext(&font,-len/2.0f,box.y+0.2f,"[ PAUSED ]");
		struct{float y;const char *name;}item[]={
			{-0.5f,"RESUME"},
			{-0.0f,"MAIN MENU"},
			{0.5f,"QUIT"}
		};
		for(int i=0;i<3;i++){
			len=textlen(&font2,item[i].name);
			float x=-len/2.0f;
			if(global->crosshair.x>x&&global->crosshair.y>item[i].y&&
			global->crosshair.x<x+len&&global->crosshair.y<item[i].y+font2.fontsize){
				if(global->click){
					global->click=false;
					if(i==0)goto PAUSEMENU_END;
					else if(i==1){
						global->showmenu=true;
						goto PAUSEMENU_END;
					}
					else if(i==2)message.show=true;
				}
				glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
			}
			drawtext(&font2,x,item[i].y,item[i].name);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		}
		if(message.show){
			message.show=false;
			if(popup(global,hdc,timestep,message.m,1))PostMessage(global->hwnd,WM_CLOSE,1,0);
		}
		if(global->key_esc){
			global->key_esc=false;
			break;
		}
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	PAUSEMENU_END:
	glDeleteTextures(1,&texture);
}
void gameover(struct global *global,HDC hdc,long long *timestep){
	global->showmenu=true;
	glUniform1f(global->uniform.rot,0.0f);
	unsigned texture=screenshot(global->device.right,global->device.bottom,5);
	FONTPROFILE font1={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.4f},
	font2={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.28f};
	float width=textlen(&font2,endgame);
	struct generic box={TID_BUILDMENU,-width/2.0f,-3.0f,width+0.8f,6.0f,0.0f},xbutton={TID_X,box.x+box.w-0.5f,box.y+0.2f,0.3f,0.3f,0.0f};
	while(global->running){
		long long start=counter();
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0f);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
		draw(global,&box);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_X].object);
		if(mouseover(global,&xbutton)){
			if(global->click){
				global->click=false;
				break;
			}
			glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		}
		draw(global,&xbutton);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		char *youfailed="YOU'RE FIRED";
		drawtext(&font1,-textlen(&font1,youfailed)/2.0f,box.y+0.4f,youfailed);
		drawtext(&font2,box.x+0.4f,box.y+1.0f,endgame);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		struct{float y;const char *name;}item[]={
			{1.0f,"MAIN MENU"},
			{1.6f,"QUIT"}
		};
		for(int i=0;i<2;i++){
			float len=textlen(&font1,item[i].name),x=-len/2.0f;
			if(global->crosshair.x>x&&global->crosshair.y>item[i].y&&global->crosshair.x<x+len&&global->crosshair.y<item[i].y+font1.fontsize){
				if(global->click){
					global->click=false;
					if(i==0)goto GAMEOVER_END;
					else if(i==1)PostMessage(global->hwnd,WM_CLOSE,1,0);
				}
				glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
			}
			drawtext(&font1,x,item[i].y,item[i].name);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		}
		if(global->wave-1>global->highscore[global->background.texture-TID_LEVEL1]){
			char *message=malloc(130);
			sprintf(message,"\tYou got a highscore!\n\nYour new record of %d completed waves beat your\nprevious record of %d!",
			global->wave-1,global->highscore[global->background.texture-TID_LEVEL1]);
			popup(global,hdc,timestep,message,0);
			free(message);
			global->highscore[global->background.texture-TID_LEVEL1]=global->wave-1;
			global->wave=0;
			FILE *file=fopen(global->highscorepath,"wb");
			if(file!=NULL){
				fwrite(global->highscore,sizeof(int),6,file);
				fclose(file);
			}
		}
		if(global->key_esc){
			global->key_esc=false;
			break;
		}
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	GAMEOVER_END:
	glDeleteTextures(1,&texture);
}
void turretmenu(struct global *global,HDC hdc,long long *timestep,struct turret *turret){
	unsigned texture=screenshot(global->device.right,global->device.bottom,5);
	glUniform1f(global->uniform.rot,0.0f);
	FONTPROFILE font={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.25f};
	struct generic box={TID_BUILDMENU,-3.0f,-2.0f,6.0f,4.0f,0.0f},xbutton={TID_X,box.x+box.w-0.5f,box.y+0.2f,0.3f,0.3f,0.0f};
	char *tinfo=malloc(41);
	while(global->running){
		long long start=counter();
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0f);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
		draw(global,&box);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_X].object);
		if(mouseover(global,&xbutton)){
			if(global->click){
				global->click=false;
				break;
			}
			glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		}
		draw(global,&xbutton);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		drawtext(&font,box.x+0.2,box.y+0.2f,"TYPE:\nDAMAGE:\nFIRE RATE:\nRANGE:\nRESALE VALUE:");
		sprintf(tinfo,"%s\n%d\n%.1f/second\n%.1f\n$%d",
		turret->stats.name,turret->stats.damage,60.0f/turret->stats.firerate,turret->stats.range,(int)(turret->stats.price/1.5f));
		drawtext(&font,0.0f,box.y+0.2f,tinfo);
		char *item="SELL";
		float len=textlen(&font,item),x=box.x+0.2,y=box.y+box.h-0.45f;
		if(global->crosshair.x>x&&global->crosshair.y>y&&global->crosshair.x<x+len&&global->crosshair.y<y+font.fontsize){
			if(global->click){
				global->click=false;
				for(struct turret *t=global->turretlist,*prevturret=NULL;t!=NULL;){
					if(turret==t){
						global->money+=turret->stats.price/1.5f;
						if(prevturret!=NULL)prevturret->next=t->next;
						else global->turretlist=t->next;
						free(t);
						break;
					}
					prevturret=t;
					t=t->next;
				}
				break;
			}
			glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		}
		drawtext(&font,x,y,item);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		if(global->key_esc){
			global->key_esc=false;
			break;
		}
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	free(tinfo);
	glDeleteTextures(1,&texture);
}
int selectlevel(struct global *global,HDC hdc,long long *timestep){
	unsigned texture=screenshot(global->device.right,global->device.bottom,5);
	glUniform1f(global->uniform.rot,0.0f);
	FONTPROFILE font1={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.23f},
	font2={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.3f};
	struct generic box={TID_BUILDMENU,-5.0f,-3.0f,10.0f,6.0f,0.0f},xbutton={TID_X,box.x+box.w-0.5f,box.y+0.2f,0.3f,0.3f,0.0f};
	struct{float x,y;int texture;const char *name;}item[]={
		{-3.2f,-1.5f,TID_LEVEL1,"Smog"},
		{-0.75f,-1.5f,TID_LEVEL2,"Night"},
		{-3.2f,0.0f,TID_LEVEL3,"Purple Princess\nPlupplepup"},
		{-0.75f,0.0f,TID_LEVEL4,"The Void"},
		{1.7f,-1.5f,TID_LEVEL5,"Cyber"},
		{1.7f,0.0f,TID_LEVEL6,"Sky"}
	};
	int ret=false;
	while(global->running){
		long long start=counter();
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0f);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
		draw(global,&box);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_X].object);
		if(mouseover(global,&xbutton)){
			if(global->click){
				global->click=false;
				ret=false;
				break;
			}
			glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		}
		draw(global,&xbutton);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		
		char *selectalevel="Select A Level:";
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		drawtext(&font2,-textlen(&font2,selectalevel)/2.0f,box.y+0.2f,selectalevel);
		for(int i=0;i<6;i++){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[item[i].texture].object);
			struct generic button={item[i].texture,item[i].x,item[i].y,1.5f,0.84375f,0.0f};
			if(mouseover(global,&button)){
				if(global->click){
					global->click=false;
					global->background.texture=item[i].texture;
					if(i==0)global->waypoint=level1waypoint;
					else if(i==1)global->waypoint=level2waypoint;
					else if(i==2)global->waypoint=level3waypoint;
					else if(i==3)global->waypoint=level4waypoint;
					else if(i==4)global->waypoint=level5waypoint;
					else if(i==5)global->waypoint=level6waypoint;
					ret=true;
				}
				glUniform3f(global->uniform.rgb,1.8f,1.8f,1.8f);
			}
			draw(global,&button);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
			drawtext(&font1,button.x,button.y+button.h+0.1f,item[i].name);
		}
		if(ret){
			char message[200];
			int index=global->background.texture-TID_LEVEL1;
			char levelname[36];
			for(int i=0;i<strlen(item[index].name)+1;i++)levelname[i]=item[index].name[i]=='\n'?' ':item[index].name[i];
			sprintf(message,"\tYou are about to play '%s'\nThe most waves you have completed on %s is %d.\n\nStart New Game?",
			levelname,levelname,global->highscore[index]);
			int result=popup(global,hdc,timestep,message,1);
			if(result)break;
			ret=false;
		}
		if(global->key_esc){
			global->key_esc=false;
			ret=false;
			break;
		}
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	glDeleteTextures(1,&texture);
	return ret;
}
void settings(struct global *global,HDC hdc,long long *timestep){
	unsigned texture=screenshot(global->device.right,global->device.bottom,5);
	glUniform1f(global->uniform.rot,0.0f);
	struct generic box={TID_BUILDMENU,-4.5f,-3.0f,9.0f,6.0f,0.0},xbutton={TID_X,box.x+box.w-0.5f,box.y+0.2f,0.3f,0.3f,0.0f};
	FONTPROFILE font1={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.4f},
	font2={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.3f};
	int newproc=false;
	while(global->running){
		long long start=counter();
		const char *message=NULL;
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0f);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
		draw(global,&box);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_X].object);
		if(mouseover(global,&xbutton)){
			if(global->click){
				global->click=false;
				break;
			}
			glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		}
		draw(global,&xbutton);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		char *header="SETTINGS";
		drawtext(&font1,-textlen(&font1,header)/2.0f,box.y+0.4f,header);
		struct{float y;const char *name;}item[]={
			{-1.0f,global->vsync?"V-SYNC: ON":"V-SYNC: OFF"},
			{0.0f,global->windowed?"WINDOWED MODE: ON":"WINDOWED MODE: OFF"}
		};
		for(int i=0;i<2;i++){
			float len=textlen(&font2,item[i].name),x=-len/2.0;
			if(global->crosshair.x>x&&global->crosshair.y>item[i].y&&global->crosshair.x<x+len&&global->crosshair.y<item[i].y+font2.fontsize){
				if(global->click){
					global->click=false;
					if(i==0){
						global->vsync=!global->vsync;
						if(wglSwapIntervalEXT!=NULL)wglSwapIntervalEXT(global->vsync);
					}
					else if(i==1){
						global->windowed=!global->windowed;
						message="The game will need to be restarted for\nthis change to take effect.\n\nRestart now?";
					}
				}
				glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
			}
			drawtext(&font2,x,item[i].y,item[i].name);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		}
		if(global->key_esc){
			global->key_esc=false;
			break;
		}
		if(message!=NULL){
			if(popup(global,hdc,timestep,message,1)){
				PostMessage(global->hwnd,WM_CLOSE,1,0);
				newproc=true;
			}
			message=NULL;
		}
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	FILE *file=fopen(global->settingspath,"wb");
	if(file!=NULL){
		fwrite(&global->vsync,sizeof(int),1,file);
		fwrite(&global->windowed,sizeof(int),1,file);
		fclose(file);
	}
	if(newproc){
		char *filename=malloc(151);
		GetModuleFileName(NULL,filename,150);
		ShellExecute(NULL,"open",filename,NULL,NULL,SW_SHOW);
		free(filename);
	}
	glDeleteTextures(1,&texture);
}
int popup(struct global *global,HDC hdc,long long *timestep,const char *text,int id){
	unsigned texture=screenshot(global->device.right,global->device.bottom,5);
	glUniform1f(global->uniform.rot,0.0f);
	FONTPROFILE font={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.27f};
	float width=textlen(&font,text)+0.5f,height=textheight(&font,text)+1.2f;
	struct generic box={TID_POPUP,-width/2.0f,-height/2.0f,width,height,0.0f};
	while(global->running){
		long long start=counter();
		
		glBindTexture(GL_TEXTURE_2D,texture);
		glUniform4f(global->uniform.texcoords,0.0f,1.0f,0.0f,1.0f);
		glUniform2f(global->uniform.vector,global->rect.left,global->rect.top);
		glUniform2f(global->uniform.size,global->rect.right*2.0f,global->rect.bottom*2.0f);
		glDrawArrays(GL_TRIANGLE_STRIP,0,4);
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_POPUP].object);
		draw(global,&box);
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		drawtext(&font,box.x+0.2f,box.y+0.2f,text);
		if(id==0){
			const char *ok="OK";
			float length=textlen(&font,ok),height=textheight(&font,ok),
			x=-length/2.0f,y=box.y+box.h-0.05f-font.fontsize;
			if(global->crosshair.x>x&&global->crosshair.y>y&&global->crosshair.x<x+length&&global->crosshair.y<y+height){
				glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
				if(global->click){
					global->click=false;
					glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0);
					goto POPUP_SUB1;
				}
			}	
			else glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
			drawtext(&font,x,y,ok);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		}
		else if(id==1){
			struct{float y;const char *name;}item[]={
				{box.y+box.h-font.fontsize-0.4f,"Yes"},
				{box.y+box.h-font.fontsize-0.1f,"No"}
			};
			for(int i=0;i<2;i++){
				float length=textlen(&font,item[i].name),x=-length/2.0f;
				if(global->crosshair.x>x&&global->crosshair.y>item[i].y&&
				global->crosshair.x<x+length&&global->crosshair.y<item[i].y+font.fontsize){
					if(global->click){
						global->click=false;
						if(i==0)goto POPUP_SUB1;
						else if(i==1)goto POPUP_SUB2;
					}
					glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
				}
				drawtext(&font,x,item[i].y,item[i].name);
				glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
			}
		}
		if(global->key_esc){
			global->key_esc=false;
			goto POPUP_SUB2;
		}
		SwapBuffers(hdc);
		while(counter()-start<*timestep);
	}
	POPUP_SUB1:
		glDeleteTextures(1,&texture);
		return true;
	POPUP_SUB2:
		glDeleteTextures(1,&texture);
		return false;
}int menu_line=__LINE__;
