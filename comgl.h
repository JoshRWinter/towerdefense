#ifndef COMGL
#define COMGL
#include "glext.h"
#include "wglext.h"
PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;
PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;
PFNGLGENBUFFERSPROC glGenBuffers;
PFNGLDELETEBUFFERSPROC glDeleteBuffers;
PFNGLBINDBUFFERPROC glBindBuffer;
PFNGLBUFFERDATAPROC glBufferData;
PFNGLINVALIDATEBUFFERDATAPROC glInvalidateBufferData;
PFNGLCREATESHADERPROC glCreateShader;
PFNGLDELETESHADERPROC glDeleteShader;
PFNGLSHADERSOURCEPROC glShaderSource;
PFNGLCOMPILESHADERPROC glCompileShader;
PFNGLCREATEPROGRAMPROC glCreateProgram;
PFNGLDELETEPROGRAMPROC glDeleteProgram;
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLDETACHSHADERPROC glDetachShader;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;
PFNGLTEXSTORAGE2DPROC glTexStorage2D;
PFNGLPRIMITIVERESTARTINDEXPROC glPrimitiveRestartIndex;
PFNGLUNIFORM1IPROC glUniform1i;
PFNGLUNIFORM1FPROC glUniform1f;
PFNGLUNIFORM2FPROC glUniform2f;
PFNGLUNIFORM3FPROC glUniform3f;
PFNGLUNIFORM4FPROC glUniform4f;
PFNGLUNIFORM4FVPROC glUniform4fv;
PFNGLUNIFORM3FVPROC glUniform3fv;
PFNGLUNIFORM2FVPROC glUniform2fv;
PFNGLUNIFORM1FVPROC glUniform1fv;
PFNGLGETUNIFORMFVPROC glGetUniformfv;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
struct packtexture{
	unsigned object;
	unsigned size;
	float pinch[4];
};
struct pack{
	struct packtexture *texture;
	unsigned count;
};
typedef struct{
	unsigned position;
	void *stream;
}RBUFFER;
typedef struct{
	unsigned vector,size,texcoords;
	char *fontbinary;
	float fontsize;
}FONTPROFILE;
#define RBSEEK_SET 1
#define RBSEEK_CUR 2
RBUFFER *rbopen(void*);
void rbclose(RBUFFER*);
void rbread(void*,unsigned,unsigned,RBUFFER*);
void rbwrite(void*,unsigned,unsigned,RBUFFER*);
void rbseek(RBUFFER*,int,int);
void drawtext(FONTPROFILE*,float,float,const char*);
float textlen(FONTPROFILE*,const char*);
float textheight(FONTPROFILE*,const char*);
char *loadfontbinary(LPCTSTR);
int loadpack(struct pack*,const char*,const char*);
int loadpackrsrc(struct pack*,LPCTSTR,const char*);
void destroypack(struct pack*);
void initfunctionpointers();
unsigned initshaders(HWND,LPCTSTR,LPCTSTR);
float *initortho(float*,float,float,float,float,float,float);
PIXELFORMATDESCRIPTOR initpfd();
int randomint(int,int);
int inputbox(HWND,LPCTSTR,char*,int);
float zerof(float*,float);
float targetf(float*,float,float);
unsigned screenshot(int,int,int);
#endif