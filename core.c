#include <windows.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "comgl.h"
#include "defs.h"
extern const char *howtoplay;
int initfunctionpointers2(HWND);
void core(void *p){
	srand(time(NULL));
	struct global *global=p;
	init(global);
	LARGE_INTEGER frequency;
	long long timestep,start;
	QueryPerformanceFrequency(&frequency);
	timestep=frequency.QuadPart/60;
	HDC hdc=GetDC(global->hwnd);
	PIXELFORMATDESCRIPTOR pfd=initpfd();
	SetPixelFormat(hdc,ChoosePixelFormat(hdc,&pfd),&pfd);
	HGLRC temphrc=wglCreateContext(hdc);
	wglMakeCurrent(hdc,temphrc);
	initfunctionpointers2(global->hwnd);
	int attribs[]={WGL_CONTEXT_MAJOR_VERSION_ARB,3,WGL_CONTEXT_MINOR_VERSION_ARB,3,0};
	HGLRC hrc=wglCreateContextAttribsARB(hdc,NULL,attribs);
	wglMakeCurrent(hdc,hrc);
	wglDeleteContext(temphrc);
	if(hrc==NULL){
		ReleaseDC(global->hwnd,hdc);
		global->quit=true;
		MessageBox(global->hwnd,"This application requires video hardware that supports at least OpenGL 3.3","Error",MB_ICONERROR);
		PostMessage(global->hwnd,WM_CLOSE,1,0);
		return;
	}
	if(wglSwapIntervalEXT!=NULL)wglSwapIntervalEXT(global->vsync);
	if(!loadpackrsrc(&global->assets,MAKEINTRESOURCE(3),NULL))MessageBox(global->hwnd,"Texture init error","Error",MB_ICONERROR);
	unsigned program=initshaders(global->hwnd,MAKEINTRESOURCE(4),MAKEINTRESOURCE(5));
	glUseProgram(program);
	global->uniform.vector=glGetUniformLocation(program,"vector");
	global->uniform.size=glGetUniformLocation(program,"size");
	global->uniform.texcoords=glGetUniformLocation(program,"texcoords");
	global->uniform.rot=glGetUniformLocation(program,"rot");
	global->uniform.lightcount=glGetUniformLocation(program,"lightcount");
	global->uniform.lightvector=glGetUniformLocation(program,"lightvector");
	global->uniform.lightcolor=glGetUniformLocation(program,"lightcolor");
	global->uniform.lightrender=glGetUniformLocation(program,"lightrender");
	global->uniform.rgb=glGetUniformLocation(program,"rgb");
	global->uniform.highlightvalid=glGetUniformLocation(program,"highlightvalid");
	global->uniform.highlight=glGetUniformLocation(program,"highlight");
	global->uniform.hvector=glGetUniformLocation(program,"hvector");
	global->uniform.hrange=glGetUniformLocation(program,"hrange");
	global->uniform.projection=glGetUniformLocation(program,"projection");
	int canvas=glGetUniformLocation(program,"canvas");
	glUniform1i(global->uniform.highlight,false);
	glUniform1i(global->uniform.highlightvalid,false);
	glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
	glUniform4f(canvas,global->device.right,global->device.bottom,global->rect.right,global->rect.bottom);
	float matrix[16];
	initortho(matrix,global->rect.left,global->rect.right,global->rect.bottom,global->rect.top,-1.0f,1.0f);
	glUniformMatrix4fv(global->uniform.projection,1,false,matrix);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	unsigned vbo,vao;
	glGenBuffers(1,&vbo);
	glGenVertexArrays(1,&vao);
	glBindBuffer(GL_ARRAY_BUFFER,vbo);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,2,GL_FLOAT,false,0,NULL);
	const float verts[]={-0.5f,-0.5f,-0.5f,0.5f,0.5f,-0.5f,0.5f,0.5f};
	glBufferData(GL_ARRAY_BUFFER,sizeof(float)*8,verts,GL_STATIC_DRAW);
	FONTPROFILE hud1={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.3f},
	hud2={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.2f},
	debugfp={global->uniform.vector,global->uniform.size,global->uniform.texcoords,global->mainfont,0.15f};
	struct{int health;float x,y;}info={false};
	while(global->running){
		start=counter();
		if(global->showmenu){
			global->showmenu=false;
			mainmenu(global,hdc,&timestep);
			continue;
		}
		if(!global->rest){
			if(!global->spawntimer&&global->remaining){
				newenemy(global);
				global->spawntimer=randomint(10,35);
				global->remaining--;
			}
			else if(global->spawntimer)global->spawntimer--;
		}
		else{
			if(global->rest==REST)savegame(global);
			global->rest--;
			if(!global->rest)nextwave(global);
		}
		
		struct turret *showtinfo=NULL;
		int lightcount=0;
		float lightvector[LIGHTCOUNT*2],lightcolor[LIGHTCOUNT*3];
		for(struct laser *laser=global->laserlist;laser!=NULL&&lightcount<LIGHTCOUNT;laser=laser->next){
			if(lightcount>=LIGHTCOUNT)break;
			lightvector[(lightcount*2)]=laser->x+(LASER_SIZE/2.0);
			lightvector[(lightcount*2)+1]=laser->y+(LASER_SIZE/2.0f);
			float rgb[3];
			lasercolor(laser->parenttype,rgb);
			lightcolor[(lightcount*3)]=rgb[0];
			lightcolor[(lightcount*3)+1]=rgb[1];
			lightcolor[(lightcount*3)+2]=rgb[2];
			lightcount++;
		}
		glUniform1i(global->uniform.lightrender,true);
		glUniform1i(global->uniform.lightcount,lightcount);
		glUniform2fv(global->uniform.lightvector,lightcount,lightvector);
		glUniform3fv(global->uniform.lightcolor,lightcount,lightcolor);

		if(global->turretplace.texture!=-1){
			struct stats stats;
			getstats(texturetype(global->turretplace.texture),&stats);
			if(global->turretplace.next==NULL)glUniform1i(global->uniform.highlightvalid,false);
			else glUniform1i(global->uniform.highlightvalid,true);
			glUniform1i(global->uniform.highlight,true);
			glUniform2f(global->uniform.hvector,global->turretplace.x+(TOWER_SIZE/2.0f),global->turretplace.y+(TOWER_SIZE/2.0f));
			glUniform1f(global->uniform.hrange,stats.range);
		}
		else for(struct turret *turret=global->turretlist;turret!=NULL;turret=turret->next)
			if(mouseover(global,turret)){
				glUniform1i(global->uniform.highlightvalid,false);
				glUniform1i(global->uniform.highlight,true);
				glUniform2f(global->uniform.hvector,turret->x+(TOWER_SIZE/2.0f),turret->y+(TOWER_SIZE/2.0f));
				glUniform1f(global->uniform.hrange,turret->stats.range);
				break;
			}
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[global->background.texture].object);
		draw(global,&global->background);
		glUniform1i(global->uniform.highlight,false);
		glUniform1i(global->uniform.lightrender,false);
		
		int step=0;
		while(global->pilot.texture!=0){
			//glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_X].object);
			if(step==0){
				newpath(global,global->pilot.x+(global->pilot.w/2.0f),global->pilot.y+(global->pilot.h/2.0f));
				step=2;
			}
			else step--;
			targetf(&global->pilot.x,fabs(global->pilot.xup),global->pilot.xtarget);
			targetf(&global->pilot.y,fabs(global->pilot.yup),global->pilot.ytarget);
			struct generic point={0,global->pilot.xtarget,global->pilot.ytarget,global->pilot.w,global->pilot.h};
			if(collide(&point,&global->pilot)){
				newpath(global,global->pilot.x+(global->pilot.w/2.0f),global->pilot.y+(global->pilot.h/2.0f));
				if(!global->waypoint(&global->pilot))global->pilot.texture=0;
			}
			//draw(global,&global->pilot);
		}
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_PARTICLE].object);
		for(struct particle *particle=global->particlelist,*prevparticle=NULL;particle!=NULL;){
			particle->x+=particle->xup;
			particle->y+=particle->yup;
			particle->rot+=particle->rotup;
			const float retard=0.008f;
			zerof(&particle->xup,retard);
			zerof(&particle->yup,retard);
			zerof(&particle->rotup,retard);
			if(particle->xup==0.0f&&particle->yup==0.0f)particle->rotup=0.0f;
			glUniform3fv(global->uniform.rgb,1,particle->rgb);
			draw(global,particle);
			if(particle->life--<0){
				zerof(&particle->w,0.02f);
				zerof(&particle->h,0.02f);
				particle->x+=0.01f;
				particle->y+=0.01f;
				if(particle->w==0.0f){
					if(prevparticle!=NULL)prevparticle->next=particle->next;
					else global->particlelist=particle->next;
					void *temp=particle->next;
					free(particle);
					particle=temp;
					continue;
				}
			}
			prevparticle=particle;
			particle=particle->next;
		}
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		
		for(struct turret *turret=global->turretlist;turret!=NULL;){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[turret->texture].object);
			draw(global,turret);
			if(global->enemylist!=NULL){
				turret->target=global->enemylist;
				float dist=sqrtf(powf((turret->target->x+(turret->target->w/2.0f))-(turret->x+(turret->w/2.0f)),2)+
				powf((turret->target->y+(turret->target->h/2.0f))-(turret->y+(turret->h/2.0f)),2));
				for(struct enemy *enemy=global->enemylist->next;enemy!=NULL;enemy=enemy->next){
					float dist2=sqrtf(powf((enemy->x+(enemy->w/2.0f))-(turret->x+(turret->w/2.0f)),2)+
					powf((enemy->y+(enemy->h/2.0f))-(turret->y+(turret->h/2.0f)),2));
					if(dist2<dist){
						dist=dist2;
						turret->target=enemy;
					}
				}
				if(dist<turret->stats.range&&!turret->reload){
					newlaser(global,turret);
					turret->reload=turret->stats.firerate;
				}
				else if(turret->reload)turret->reload--;
			}
			if(mouseover(global,turret)&&global->click){
				global->click=false;
				showtinfo=turret;
			}
			turret=turret->next;
		}
		
		if(global->turretplace.texture!=-1){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[global->turretplace.texture].object);
			global->turretplace.x=global->crosshair.x-(global->turretplace.w/2.0f);
			global->turretplace.y=global->crosshair.y-(global->turretplace.h/2.0f);
			glUniform3f(global->uniform.rgb,1.4f,1.4f,1.4f);
			draw(global,&global->turretplace);
			glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
			char *popup="Right-Click to Cancel";
			drawtext(&hud2,global->turretplace.x+(TOWER_SIZE/2.0f)-textlen(&hud2,popup)/2.0f,global->turretplace.y+TOWER_SIZE+0.05f,popup);
			int valid=true;
			for(struct turret *turret=global->turretlist;turret!=NULL;turret=turret->next)
				if(collide(&global->turretplace,turret)){
					valid=false;
					break;
				}
			for(struct generic *path=global->pathlist;path!=NULL;path=path->next)
				if(collide(&global->turretplace,path)){
					valid=false;
					break;
				}
			if(global->turretplace.x<global->rect.left||global->turretplace.x+global->turretplace.w>global->rect.right||
			global->turretplace.y<global->rect.top||global->turretplace.y+global->turretplace.h>global->rect.bottom)valid=false;
			struct stats stats;
			getstats(texturetype(global->turretplace.texture),&stats);
			if(global->money<stats.price)valid=false;
			if(valid)global->turretplace.next=(void*)1;
			else global->turretplace.next=NULL;
			if(global->click){
				global->click=false;
				if(valid){
					newturret(global,global->crosshair.x-(global->turretplace.w/2.0f),
					global->crosshair.y-(global->turretplace.h/2.0f),texturetype(global->turretplace.texture));
					global->money-=stats.price;
				}
			}
			if(global->rclick){
				global->rclick=false;
				global->turretplace.texture=-1;
			}
		}
		
		int hover=false;
		for(struct enemy *enemy=global->enemylist,*prevenemy=NULL;enemy!=NULL;){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[enemy->texture].object);
			targetf(&enemy->x,fabs(enemy->xup),enemy->xtarget);
			targetf(&enemy->y,fabs(enemy->yup),enemy->ytarget);
			targetf(&enemy->rot,enemy->texture==TID_BOSS?0.05f:0.15f,enemy->rottarget);
			draw(global,enemy);
			if(mouseover(global,enemy)){
				hover=true;
				info.health=enemy->health;
				info.x=enemy->x;
				info.y=enemy->y;
			}
			struct generic point={0,enemy->xtarget,enemy->ytarget,
			enemy->texture==TID_BOSS?0.05f:enemy->w,enemy->texture==TID_BOSS?0.05f:enemy->h};
			if(collide(enemy,&point)&&!global->waypoint(enemy)){
				for(struct laser *laser=global->laserlist,*prevlaser=NULL;laser!=NULL;){
					if(laser->target==enemy){
						if(prevlaser!=NULL)prevlaser->next=laser->next;
						else global->laserlist=laser->next;
						void *temp=laser->next;
						free(laser);
						laser=temp;
						continue;
					}
					prevlaser=laser;
					laser=laser->next;
				}
				if(prevenemy!=NULL)prevenemy->next=enemy->next;
				else global->enemylist=enemy->next;
				void *temp=enemy->next;
				free(enemy);
				enemy=temp;
				if(!global->remaining&&global->enemylist==NULL)global->rest=REST;
				global->leaked++;
				continue;
			}
			int stop=false;
			for(struct laser *laser=global->laserlist,*prevlaser=NULL;laser!=NULL;){
				if(laser->kill){
					prevlaser=laser;
					laser=laser->next;
					continue;
				}
				if(collide(enemy,laser)){
					laser->kill=true;
					enemy->health-=laser->damage;
					if(laser->parenttype==POISON_TOWER){
						if(enemy->speed<150.0f&&enemy->texture!=TID_BOSS){
							enemy->xup/=1.1f;
							enemy->yup/=1.1f;
							enemy->speed*=1.1f;
						}
					}
					if(enemy->health<1){
						global->money+=global->money<400?randomint(5,9):randomint(2,4);
						if(enemy->texture==TID_BOSS)global->money+=randomint(100,150);
						for(struct laser *laser2=global->laserlist;laser2!=NULL;laser2=laser2->next)
							if(laser2->target==enemy)laser2->kill=true;
						newparticle(global,enemy->x+(enemy->w/2.0f),enemy->y+(enemy->h/2.0f),enemy->texture);
						if(prevenemy!=NULL)prevenemy->next=enemy->next;
						else global->enemylist=enemy->next;
						void *temp=enemy->next;
						free(enemy);
						enemy=temp;
						if(!global->remaining&&global->enemylist==NULL)global->rest=REST;
						stop=true;
						break;
					}
					continue;
				}
				prevlaser=laser;
				laser=laser->next;
			}
			if(stop)continue;
			prevenemy=enemy;
			enemy=enemy->next;
		}
		
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_LASER].object);
		for(struct laser *laser=global->laserlist,*prevlaser=NULL;laser!=NULL;){
			if(laser->kill||laser->x+LASER_SIZE<global->rect.left||laser->x>global->rect.right||
			laser->y+LASER_SIZE<global->rect.top||laser->y>global->rect.bottom){
				if(prevlaser!=NULL)prevlaser->next=laser->next;
				else global->laserlist=laser->next;
				void *temp=laser->next;
				free(laser);
				laser=temp;
				continue;
			}
			laser->rot=-atan2f((laser->y+(LASER_SIZE/2.0))-(laser->target->y+(laser->target->h/2.0)),
			(laser->x+(laser->w/2.0f))-(laser->target->x+(laser->target->w/2.0)));
			laser->xup=-cosf(-laser->rot)/laser->speed;
			laser->yup=-sinf(-laser->rot)/laser->speed;
			laser->x+=laser->xup;
			laser->y+=laser->yup;
			draw(global,laser);
			prevlaser=laser;
			laser=laser->next;
		}
		
		if(global->showpathblocks){
			glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDMENU].object);
			for(struct generic *path=global->pathlist;path!=NULL;path=path->next)
				draw(global,path);
		}
		if(global->background.texture==TID_LEVEL4)glUniform3f(global->uniform.rgb,0.0f,0.0f,0.0f);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_FONT].object);
		glUniform1f(global->uniform.rot,0.0f);
		char hudtext[85];
		sprintf(hudtext,"LEAKED: %d/%d    $%d",global->leaked,MAXLEAKED,global->money);
		drawtext(&hud1,-3.8f,global->rect.top,hudtext);
		sprintf(hudtext,"WAVE: %d",global->wave);
		drawtext(&hud1,1.0f,global->rect.top,hudtext);
		sprintf(hudtext,"Get Ready! %.1f",global->rest/60.0f);
		float hudtextlen=textlen(&hud1,hudtext);
		if(global->rest)drawtext(&hud1,-hudtextlen/2.0f,-4.0f,hudtext);
		if(global->showdebuginfo){
			sprintf(hudtext,"lights:%2d\nX:%+.2f Y:%+.2f\nbasehealth:%.1f\nremaining:%d",
			lightcount,global->crosshair.x,global->crosshair.y,(global->healthadd)*HEALTHMULT,global->remaining);
			drawtext(&debugfp,global->rect.left,global->rect.top,hudtext);
		}
		if(global->background.texture==TID_LEVEL4)glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		if(hover&&global->turretplace.texture==-1){
			sprintf(hudtext,"%dhp",info.health);
			drawtext(&hud1,info.x,info.y,hudtext);
		}
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_BUILDTAB].object);
		if(mouseover(global,&global->buildtab))glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		draw(global,&global->buildtab);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_PAUSETAB].object);
		if(mouseover(global,&global->pausetab))glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		draw(global,&global->pausetab);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		glBindTexture(GL_TEXTURE_2D,global->assets.texture[TID_QTAB].object);
		if(mouseover(global,&global->qtab))glUniform3f(global->uniform.rgb,1.0f,0.0f,0.0f);
		draw(global,&global->qtab);
		glUniform3f(global->uniform.rgb,1.0f,1.0f,1.0f);
		
		if(global->leaked>=MAXLEAKED){
			FILE *file=fopen(global->path,"wb");
			if(file!=NULL)fclose(file);
			gameover(global,hdc,&timestep);
		}
		if(showtinfo&&global->turretplace.texture==-1){
			turretmenu(global,hdc,&timestep,showtinfo);
			showtinfo=NULL;
		}
		if(mouseover(global,&global->buildtab)){
			buildmenu(global,hdc,&timestep);
		}
		else if(mouseover(global,&global->pausetab)&&global->click){
			global->click=false;
			pausemenu(global,hdc,&timestep);
		}
		else if(mouseover(global,&global->qtab)&&global->click){
			global->click=false;
			popup(global,hdc,&timestep,howtoplay,0);
		}
		if(global->key_esc){
			global->key_esc=false;
			pausemenu(global,hdc,&timestep);
		}
		SwapBuffers(hdc);
		while(counter()-start<timestep);
	}
	reset(global);
	free(global->bauhaus);
	free(global->mainfont);
	destroypack(&global->assets);
	glDeleteProgram(program);
	glDeleteBuffers(1,&vbo);
	glDeleteVertexArrays(1,&vao);
	wglMakeCurrent(hdc,NULL);
	wglDeleteContext(hrc);
	ReleaseDC(global->hwnd,hdc);
	global->quit=true;
}
void init(struct global *global){
	ExpandEnvironmentStrings("%appdata%\\TowerDefense\\savegame.dat",global->path,500);
	ExpandEnvironmentStrings("%appdata%\\TowerDefense\\highscores.dat",global->highscorepath,500);
	ExpandEnvironmentStrings("%appdata%\\TowerDefense\\settings.dat",global->settingspath,500);
	FILE *file=fopen(global->highscorepath,"rb");
	if(file==NULL)
		for(int i=0;i<6;i++)global->highscore[i]=0;
	else{
		fread(global->highscore,sizeof(int),6,file);
		fclose(file);
	}
	file=fopen(global->settingspath,"rb");
	if(file==NULL){
		global->vsync=true;
		global->windowed=false;
	}
	else{
		fread(&global->vsync,sizeof(int),1,file);
		fread(&global->windowed,sizeof(int),1,file);
		fclose(file);
	}
	global->bauhaus=loadfontbinary(MAKEINTRESOURCE(6));
	global->mainfont=loadfontbinary(MAKEINTRESOURCE(7));
	global->running=true;
	global->quit=false;
	global->showmenu=true;
	global->click=false;
	global->rclick=false;
	global->click=false;
	global->rclick=false;
	global->key_esc=false;
	global->showdebuginfo=false;
	global->showpathblocks=false;
	global->waypoint=NULL;
	global->crosshair.x=0.0f;
	global->crosshair.y=0.0f;
	GetClientRect(global->hwnd,&global->device);
	global->rect.right=8.0f;
	global->rect.bottom=4.5f;
	global->rect.left=-global->rect.right;
	global->rect.top=-global->rect.bottom;
	global->background.x=global->rect.left;
	global->background.y=global->rect.top;
	global->background.w=global->rect.right*2.0f;
	global->background.h=global->rect.bottom*2.0f;
	global->background.rot=0.0f;
	global->buildtab.texture=TID_BUILDTAB;
	global->buildtab.w=0.6f;
	global->buildtab.h=0.3f;
	global->buildtab.x=-global->buildtab.w/2.0f;
	global->buildtab.y=global->rect.top-0.01f;
	global->buildtab.rot=0.0f;
	global->pausetab.texture=TID_PAUSETAB;
	global->pausetab.w=0.3f;
	global->pausetab.h=0.3f;
	global->pausetab.x=-0.7f;
	global->pausetab.y=global->rect.top-0.01f;
	global->pausetab.rot=0.0f;
	global->qtab.texture=TID_QTAB;
	global->qtab.w=0.22f;
	global->qtab.h=0.3f;
	global->qtab.x=0.4f;
	global->qtab.y=global->rect.top-0.01f;
	global->qtab.rot=0.0f;
	global->pilot.w=0.3f;
	global->pilot.h=0.3f;
	global->pilot.speed=2.8f;
	global->pilot.rot=0.0f;
	global->turretplace.w=TOWER_SIZE;
	global->turretplace.h=TOWER_SIZE;
	global->turretplace.rot=0.0f;
	global->turretplace.next=NULL;
	global->enemylist=NULL;
	global->turretlist=NULL;
	global->laserlist=NULL;
	global->particlelist=NULL;
	global->pathlist=NULL;
}
void reset(struct global *global){
	for(struct enemy *enemy=global->enemylist;enemy!=NULL;){
		void *temp=enemy->next;
		free(enemy);
		enemy=temp;
	}
	global->enemylist=NULL;
	for(struct turret *turret=global->turretlist;turret!=NULL;){
		void *temp=turret->next;
		free(turret);
		turret=temp;
	}
	global->turretlist=NULL;
	for(struct laser *laser=global->laserlist;laser!=NULL;){
		void *temp=laser->next;
		free(laser);
		laser=temp;
	}
	global->laserlist=NULL;
	for(struct particle *particle=global->particlelist;particle!=NULL;){
		void *temp=particle->next;
		free(particle);
		particle=temp;
	}
	global->particlelist=NULL;
	for(struct generic *path=global->pathlist;path!=NULL;){
		void *temp=path->next;
		free(path);
		path=temp;
	}
	global->pathlist=NULL;
	global->leaked=0;
	global->turretplace.texture=-1;
	global->spawntimer=0;
	global->money=STARTING_FUNDS;
	global->healthadd=100;
	global->rest=REST-1;
	global->remaining=0;
	global->wave=0;
	global->pilot.texture=TID_X;
	global->pilot.loc=-1;
	if(global->waypoint!=NULL)global->waypoint(&global->pilot);
}
long long counter(){
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return li.QuadPart;
}int core_line=__LINE__;
