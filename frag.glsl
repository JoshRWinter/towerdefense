#version 330 core
#define LIGHTCOUNT 20
layout(origin_upper_left)in vec4 gl_FragCoord;
uniform vec4 canvas;
uniform vec3 lightcolor[LIGHTCOUNT],rgb;
uniform vec2 lightvector[LIGHTCOUNT];
uniform int lightcount;
uniform bool lightrender;
uniform bool highlight,highlightvalid;
uniform vec2 hvector;
uniform float hrange;
uniform sampler2D tex;
in vec2 ftexcoord;
out vec4 color;
void main(){
	color=texture(tex,ftexcoord)*vec4(rgb,1.0);
	if(highlight){
		vec2 coord;
		coord.x=((gl_FragCoord.x/canvas[0])*(canvas[2]*2.0))-canvas[2];
		coord.y=((gl_FragCoord.y/canvas[1])*(canvas[3]*2.0))-canvas[3];
		if(distance(coord,hvector)<hrange)
			if(highlightvalid)color+=vec4(-0.3,0.3,-0.3,0.0);
			else color+=vec4(0.1,0.1,0.1,0.0);
	}
	if(!lightrender)return;
	for(int i=0;i<lightcount;i++){
		vec2 coord;
		coord.x=((gl_FragCoord.x/canvas[0])*(canvas[2]*2.0))-canvas[2];
		coord.y=((gl_FragCoord.y/canvas[1])*(canvas[3]*2.0))-canvas[3];
		const float attenuation=1.0;
		if(abs(coord.x-lightvector[i].x)>attenuation||abs(coord.y-lightvector[i].y)>attenuation)continue;
		float d=distance(lightvector[i],coord);
		float intensity=0.6*(attenuation-d);
		if(intensity>0.0)color+=vec4(intensity*lightcolor[i].r,intensity*lightcolor[i].g,intensity*lightcolor[i].b,1.0);
	}
}