#include <windows.h>
#include <GL/gl.h>
#include <stdio.h>
#include "comgl.h"
#include "defs.h"
LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);
int _mkdir(const char*);
const char g_szClassName[]="defWindowClass";
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow){
	char settingspath[501];
	ExpandEnvironmentStrings("%appdata%\\TowerDefense",settingspath,500);
	_mkdir(settingspath);
	ExpandEnvironmentStrings("%appdata%\\TowerDefense\\settings.dat",settingspath,500);
	int windowed;
	FILE *file=fopen(settingspath,"rb");
	if(file!=NULL){
		fseek(file,sizeof(int),SEEK_SET);
		fread(&windowed,sizeof(int),1,file);
		fclose(file);
	}
	else windowed=false;
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;
	wc.cbSize=sizeof(WNDCLASSEX);
	wc.style=CS_OWNDC;
	wc.lpfnWndProc=WndProc;
	wc.cbClsExtra=0;
	wc.cbWndExtra=0;
	wc.hInstance=hInstance;
	wc.hIcon=LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(2));
	wc.hCursor=LoadCursor(GetModuleHandle(NULL),MAKEINTRESOURCE(8));//LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground=(HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName=NULL;
	wc.lpszClassName=g_szClassName;
	wc.hIconSm=LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(2));
	if(!RegisterClassEx(&wc)){
		MessageBox(NULL,"Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}
	struct global global;
	if(!windowed)
	hwnd=CreateWindowEx(0,g_szClassName,"",WS_POPUP,0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN),NULL,NULL,hInstance,&global);
	else{
		int w=GetSystemMetrics(SM_CXSCREEN)/1.25,h=GetSystemMetrics(SM_CYSCREEN)/1.25;
		int x=GetSystemMetrics(SM_CXSCREEN)-w,y=GetSystemMetrics(SM_CYSCREEN)-h;
		const char *windowtext="Tower Defense skiddly diddly bee bop boodiby la la fo lo bobbity boop";
		hwnd=CreateWindowEx(0,g_szClassName,windowtext,WS_SYSMENU|WS_MINIMIZEBOX,x/3,y/3,w,h,NULL,NULL,hInstance,&global);
	}
	if(hwnd==NULL){
		MessageBox(NULL,"Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}
	ShowWindow(hwnd,nCmdShow);
	UpdateWindow(hwnd);
	while(GetMessage(&Msg,NULL,0,0)>0){
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return Msg.wParam;
}int main_line=__LINE__;



/*---TEMPLATES----
BOOL CALLBACK AboutDlgProc(HWND,UINT,WPARAM,LPARAM);
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Msg,WPARAM wParam,LPARAM lParam)
{
    switch(Msg)
    {
        case WM_INITDIALOG:
        	return TRUE;
        case WM_COMMAND:
            switch(LOWORD(wParam))
            {
				case IDOK:
					EndDialog(hwnd, IDOK);
					break;
				case IDCANCEL:
					EndDialog(hwnd, IDCANCEL);
					break;
			}
			break;
        default:
            return FALSE;
    }
    return TRUE;
}
3 DIALOGEX 0,0,150,50
STYLE DS_MODALFRAME|WS_CAPTION|WS_SYSMENU|DS_CENTER
CAPTION ""
FONT 8, "segoe ui"
BEGIN
DEFPUSHBUTTON "&OK",1,45,28,60,15
EDITTEXT 3,25,10,100,15,ES_AUTOHSCROLL
END
*/
