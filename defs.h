#define TID_FONT 0
#define TID_FONT2 1
#define TID_MENU 2
#define TID_LEVEL1 3
#define TID_LEVEL2 4
#define TID_LEVEL3 5
#define TID_LEVEL4 6
#define TID_LEVEL5 7
#define TID_LEVEL6 8
#define TID_ENEMY1 9
#define TID_ENEMY2 10
#define TID_ENEMY3 11
#define TID_ENEMY4 12
#define TID_BOSS 13
#define TID_BUILDTAB 14
#define TID_PAUSETAB 15
#define TID_QTAB 16
#define TID_X 17
#define TID_BUILDMENU 18
#define TID_POPUP 19
#define TID_LASER 20
#define TID_PARTICLE 21
#define TID_LASERTOWER 22
#define TID_PLASMATOWER 23
#define TID_POISONTOWER 24
#define TID_ABUSIVETOWER 25
#define TID_BEARTOWER 26
#define true 1
#define false 0
#define pi 3.14159f
#define HEALTHMULT 1.24f
#define LIGHTCOUNT 20
#define STARTING_FUNDS 300
#define REST 420
#define MAXLEAKED 5
struct generic{
	int texture;
	float x,y,w,h,rot;
	struct generic *next;
};
struct enemy{
	int texture;
	float x,y,w,h,rot,rottarget,xtarget,ytarget,xup,yup,speed;
	int loc,health;
	struct enemy *next;
};
#define LASER_TOWER 100
#define PLASMA_TOWER 101
#define POISON_TOWER 102
#define ABUSIVE_TOWER 103
#define BEAR_TOWER 104
#define TOWER_SIZE 0.55f
struct stats{int price,firerate,damage;float range;const char *name;};
struct turret{
	int texture;
	float x,y,w,h,rot;
	int reload,type;
	struct stats stats;
	struct enemy *target;
	struct turret *next;
};
#define LASER_SIZE 0.1f
struct laser{
	int texture;
	float x,y,w,h,rot,xup,yup,speed;
	int parenttype,damage,kill;
	struct enemy *target;
	struct laser *next;
};
#define PARTICLE_SIZE 0.15f
struct particle{
	int texture;
	float x,y,w,h,rot,xup,yup,rotup,rgb[3];
	int life;
	struct particle *next;
};
struct global{
	char path[501],highscorepath[501],settingspath[501],*bauhaus,*mainfont;
	volatile int running,quit,click,rclick,key_esc,showdebuginfo,showpathblocks;
	int spawntimer,money,showmenu,wave,remaining,rest,healthadd,leaked,highscore[6],vsync,windowed;
	int (*waypoint)(struct enemy*);
	HWND hwnd;
	RECT device;
	struct pack assets;
	struct{float left,right,bottom,top;}rect;
	struct{float x,y;}crosshair;
	struct{int vector,size,texcoords,rot,lightcount,lightvector,lightcolor,lightrender,rgb,highlightvalid,
	highlight,hvector,hrange,projection;}uniform;
	struct generic background,buildtab,pausetab,qtab,turretplace;
	struct enemy *enemylist,pilot;
	struct turret *turretlist;
	struct laser *laserlist;
	struct particle *particlelist;
	struct generic *pathlist;
};
void savegame(struct global*);
int loadgame(struct global*);
void nextwave(struct global*);
void newpath(struct global*,float,float);
void newlaser(struct global*,struct turret*);
void newparticle(struct global*,float,float,int);
void newturret(struct global*,float,float,int);
int texturetype(int);
void lasercolor(int,float*);
int level1waypoint(struct enemy*);
int level2waypoint(struct enemy*);
int level3waypoint(struct enemy*);
int level4waypoint(struct enemy*);
int level5waypoint(struct enemy*);
int level6waypoint(struct enemy*);
void getstats(int,struct stats*);
void newenemy(struct global*);
int mouseover(struct global*,void*);
int collide(void*,void*);
int popup(struct global*,HDC,long long*,const char*,int);
int selectlevel(struct global*,HDC,long long*);
void turretmenu(struct global*,HDC,long long*,struct turret*);
void mainmenu(struct global*,HDC,long long*);
void buildmenu(struct global*,HDC,long long*);
void pausemenu(struct global*,HDC,long long*);
void gameover(struct global*,HDC,long long*);
void settings(struct global*,HDC,long long*);
void core(void*);
void init(struct global*);
void reset(struct global*);
void draw(struct global*,void*);
long long counter();
float kerning(char);
