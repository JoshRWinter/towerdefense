#include <windows.h>
#include <GL/gl.h>
#include <stdio.h>
#include "comgl.h"
#include "defs.h"
int functionextensionerror=0;
static PROC loadext(HWND hwnd,const char *name){
	PROC ext=wglGetProcAddress(name);
	if(ext==NULL){
		char *message=malloc(400);
		sprintf(message,"The following GL Extension has failed to initialize:\n\n%s\n\nPlease make sure your video drivers support OpenGL and are up to date.",name);
		functionextensionerror=1;
		MessageBox(hwnd,message,"Extension error",MB_ICONERROR);
		free(message);
		return NULL;
	}
	return ext;
}
int initfunctionpointers2(HWND hwnd){
	functionextensionerror=0;
	wglCreateContextAttribsARB=(PFNWGLCREATECONTEXTATTRIBSARBPROC)loadext(hwnd,"wglCreateContextAttribsARB");
	//wglChoosePixelFormatARB=(PFNWGLCHOOSEPIXELFORMATARBPROC)loadext(hwnd,"wglChoosePixelFormatARB");
	wglSwapIntervalEXT=(PFNWGLSWAPINTERVALEXTPROC)loadext(hwnd,"wglSwapIntervalEXT");
	glGenBuffers=(PFNGLGENBUFFERSPROC)loadext(hwnd,"glGenBuffers");
	glDeleteBuffers=(PFNGLDELETEBUFFERSPROC)loadext(hwnd,"glDeleteBuffers");
	glBindBuffer=(PFNGLBINDBUFFERPROC)loadext(hwnd,"glBindBuffer");
	glBufferData=(PFNGLBUFFERDATAPROC)loadext(hwnd,"glBufferData");
	glCreateShader=(PFNGLCREATESHADERPROC)loadext(hwnd,"glCreateShader");
	glDeleteShader=(PFNGLDELETESHADERPROC)loadext(hwnd,"glDeleteShader");
	glShaderSource=(PFNGLSHADERSOURCEPROC)loadext(hwnd,"glShaderSource");
	glCompileShader=(PFNGLCOMPILESHADERPROC)loadext(hwnd,"glCompileShader");
	glCreateProgram=(PFNGLCREATEPROGRAMPROC)loadext(hwnd,"glCreateProgram");
	glAttachShader=(PFNGLATTACHSHADERPROC)loadext(hwnd,"glAttachShader");
	glDetachShader=(PFNGLDETACHSHADERPROC)loadext(hwnd,"glDetachShader");
	glLinkProgram=(PFNGLLINKPROGRAMPROC)loadext(hwnd,"glLinkProgram");
	glUseProgram=(PFNGLLINKPROGRAMPROC)loadext(hwnd,"glUseProgram");
	glDeleteProgram=(PFNGLDELETEPROGRAMPROC)loadext(hwnd,"glDeleteProgram");
	glGetShaderInfoLog=(PFNGLGETSHADERINFOLOGPROC)loadext(hwnd,"glGetShaderInfoLog");
	glGetProgramInfoLog=(PFNGLGETPROGRAMINFOLOGPROC)loadext(hwnd,"glGetProgramInfoLog");
	glGetShaderiv=(PFNGLGETSHADERIVPROC)loadext(hwnd,"glGetShaderiv");
	glVertexAttribPointer=(PFNGLVERTEXATTRIBPOINTERPROC)loadext(hwnd,"glVertexAttribPointer");
	glEnableVertexAttribArray=(PFNGLENABLEVERTEXATTRIBARRAYPROC)loadext(hwnd,"glEnableVertexAttribArray");
	glGenVertexArrays=(PFNGLGENVERTEXARRAYSPROC)loadext(hwnd,"glGenVertexArrays");
	glBindVertexArray=(PFNGLBINDVERTEXARRAYPROC)loadext(hwnd,"glBindVertexArray");
	glDeleteVertexArrays=(PFNGLDELETEVERTEXARRAYSPROC)loadext(hwnd,"glDeleteVertexArrays");
	glTexStorage2D=(PFNGLTEXSTORAGE2DPROC)loadext(hwnd,"glTexStorage2D");
	glPrimitiveRestartIndex=(PFNGLPRIMITIVERESTARTINDEXPROC)loadext(hwnd,"glPrimitiveRestartIndex");
	glUniform1i=(PFNGLUNIFORM1IPROC)loadext(hwnd,"glUniform1i");
	glUniform1f=(PFNGLUNIFORM1FPROC)loadext(hwnd,"glUniform1f");
	glUniform2f=(PFNGLUNIFORM2FPROC)loadext(hwnd,"glUniform2f");
	glUniform3f=(PFNGLUNIFORM3FPROC)loadext(hwnd,"glUniform3f");
	glUniform4f=(PFNGLUNIFORM4FPROC)loadext(hwnd,"glUniform4f");
	glUniform4fv=(PFNGLUNIFORM4FVPROC)loadext(hwnd,"glUniform4fv");
	glUniform3fv=(PFNGLUNIFORM3FVPROC)loadext(hwnd,"glUniform3fv");
	glUniform2fv=(PFNGLUNIFORM2FVPROC)loadext(hwnd,"glUniform2fv");
	glUniform1fv=(PFNGLUNIFORM1FVPROC)loadext(hwnd,"glUniform1fv");
	glGetUniformfv=(PFNGLGETUNIFORMFVPROC)loadext(hwnd,"glGetUniformfv");
	glUniformMatrix4fv=(PFNGLUNIFORMMATRIX4FVPROC)loadext(hwnd,"glUniformMatrix4fv");
	glGetUniformLocation=(PFNGLGETUNIFORMLOCATIONPROC)loadext(hwnd,"glGetUniformLocation");
	return functionextensionerror;
}
void savegame(struct global *global){
	FILE *file=fopen(global->path,"wb");
	if(file==NULL)return;
	fwrite(&global->money,sizeof(int),1,file);
	fwrite(&global->wave,sizeof(int),1,file);
	fwrite(&global->healthadd,sizeof(int),1,file);
	fwrite(&global->leaked,sizeof(int),1,file);
	fwrite(&global->background.texture,sizeof(int),1,file);
	int turretcount=0;
	for(struct turret *turret=global->turretlist;turret!=NULL;turret=turret->next)turretcount++;
	fwrite(&turretcount,sizeof(int),1,file);
	for(struct turret *turret=global->turretlist;turret!=NULL;turret=turret->next){
		fwrite(&turret->type,sizeof(int),1,file);
		fwrite(&turret->x,sizeof(float),1,file);
		fwrite(&turret->y,sizeof(float),1,file);
	}
	fclose(file);
}
int loadgame(struct global *global){
	FILE *file=fopen(global->path,"rb");
	if(file==NULL)return false;
	fseek(file,0,SEEK_END);
	if(!ftell(file)){
		fclose(file);
		return false;
	}
	fseek(file,0,SEEK_SET);
	fread(&global->money,sizeof(int),1,file);
	fread(&global->wave,sizeof(int),1,file);
	fread(&global->healthadd,sizeof(int),1,file);
	fread(&global->leaked,sizeof(int),1,file);
	fread(&global->background.texture,sizeof(int),1,file);
	switch(global->background.texture){
		case TID_LEVEL1:
			global->waypoint=level1waypoint;
			break;
		case TID_LEVEL2:
			global->waypoint=level2waypoint;
			break;
		case TID_LEVEL3:
			global->waypoint=level3waypoint;
			break;
		case TID_LEVEL4:
			global->waypoint=level4waypoint;
			break;
		case TID_LEVEL5:
			global->waypoint=level5waypoint;
			break;
		case TID_LEVEL6:
			global->waypoint=level6waypoint;
			break;
	}
	int turretcount;
	fread(&turretcount,sizeof(int),1,file);
	for(int i=0;i<turretcount;i++){
		int id;
		float pos[2];
		fread(&id,sizeof(int),1,file);
		fread(pos,sizeof(float),2,file);
		newturret(global,pos[0],pos[1],id);
	}
	fclose(file);
	return true;
}
const char *desc(int id){
	switch(id){
		case 0:return
			"General purpose tower designed for the quick, clean elimination\n"
			"of your enemies.";
		case 1:return
			"A bit more expensive, the PLASMA TOWER will get the job done\n"
			"with a little more ehh... finesse.";
		case 2:return
			"Poison darts that will slow your enemy and make them easier\n"
			"to target.";
		case 3:return
			"This tower will abuse your opponents physically and verbally\n"
			"and will leave emotional scars on their psyche that they don't\n"
			"find until years later.";
		case 4:return
			"This tower literally fires bears.\n\n"
			"It takes a bear, shrinks it down, and launches it through the air\n"
			"and into the tender flesh of your target. The bear then grows\n"
			"back to its original size and eats the host from within.";
	}
}
void getstats(int id,struct stats *stats){
	switch(id){
		case LASER_TOWER:
			stats->name="Laser Tower";
			stats->range=2.0f;
			stats->firerate=4;
			stats->damage=4;
			stats->price=45;
			break;
		case PLASMA_TOWER:
			stats->name="Plasma Tower";
			stats->range=3.0f;
			stats->firerate=15;
			stats->damage=23;
			stats->price=65;
			break;
		case POISON_TOWER:
			stats->name="Poison Tower";
			stats->range=1.5f;
			stats->firerate=30;
			stats->damage=10;
			stats->price=100;
			break;
		case ABUSIVE_TOWER:
			stats->name="Abusive Tower";
			stats->range=4.0f;
			stats->firerate=45;
			stats->damage=320;
			stats->price=145;
			break;
		case BEAR_TOWER:
			stats->name="Bear Tower";
			stats->range=1.9;
			stats->firerate=120;
			stats->damage=610;
			stats->price=240;
			break;
	}
}
void lasercolor(int type,float *rgb){
	const float HIGH=1.0f,LOW=-0.1f;
	switch(type){
		case LASER_TOWER:
			rgb[0]=HIGH;
			rgb[1]=LOW;
			rgb[2]=LOW;
			break;
		case PLASMA_TOWER:
			rgb[0]=LOW;
			rgb[1]=LOW;
			rgb[2]=HIGH;
			break;
		case POISON_TOWER:
			rgb[0]=LOW;
			rgb[1]=HIGH;
			rgb[2]=LOW;
			break;
		case ABUSIVE_TOWER:
			rgb[0]=HIGH;
			rgb[1]=HIGH;
			rgb[2]=LOW;
			break;
		case BEAR_TOWER:
			rgb[0]=0.58f;
			rgb[1]=0.3f;
			rgb[2]=0.0f;
			break;
	}
}
void nextwave(struct global *global){
	global->wave++;
	if(global->wave>1)global->healthadd+=/*38+*/(global->wave*13);
	switch(global->wave){
		case 1:
			global->remaining=10;
			break;
		case 2:
			global->remaining=18;
			break;
		case 3:
			global->remaining=28;
			break;
		case 4:
			global->remaining=36;
			break;
		case 5:
			global->remaining=54;
			break;
		case 6:
		default:
			global->remaining=80;
			break;
	}
}
int texturetype(int i){
	switch(i){
		case TID_LASERTOWER:return LASER_TOWER;
		case TID_PLASMATOWER:return PLASMA_TOWER;
		case TID_POISONTOWER:return POISON_TOWER;
		case TID_ABUSIVETOWER:return ABUSIVE_TOWER;
		case TID_BEARTOWER:return BEAR_TOWER;
		
		case LASER_TOWER:return TID_LASERTOWER;
		case PLASMA_TOWER:return TID_PLASMATOWER;
		case POISON_TOWER:return TID_POISONTOWER;
		case ABUSIVE_TOWER:return TID_ABUSIVETOWER;
		case BEAR_TOWER:return TID_BEARTOWER;
	}
	return -1;
}
const char *endgame=
"You let too many lil dudes past the thing.\n"
"please uninstall this game from your computer now.";
const char *howtoplay=
"\tHOW TO PLAY TOWER DEFENSE:\n\n"
"- Access the build menu and the pause screen by clicking\n"
"on the tabs at the top of your screen.\n\n"
"- Buy towers and place them along the path in strategic\n"
"places in order to destroy enemies and earn money.\n\n"
"- Click on a tower to view its stats. You can also sell\n"
"the tower from this menu.\n\n"
"- If you die in this game you die in real life\n";
const char *about="TOWER DEFENSE\n\n"
"Programming and Art by Josh Winter\n"
"C99 + OpenGL 3.3";int misc_line=__LINE__;
