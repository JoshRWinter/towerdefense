#include <windows.h>
#include <GL/gl.h>
#include <process.h>
#include "comgl.h"
#include "defs.h"
LRESULT CALLBACK WndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	static struct global *global;
	switch(msg){
		case WM_CREATE:
			global=((LPCREATESTRUCT)lParam)->lpCreateParams;
			global->hwnd=hwnd;
			_beginthread(core,0,global);
			break;
		case WM_KEYDOWN:
			if(wParam==VK_ESCAPE)global->key_esc=true;
			else if(wParam==VK_F1)global->showdebuginfo=!global->showdebuginfo;
			else if(wParam==VK_F2)global->showpathblocks=!global->showpathblocks;
			break;
		case WM_LBUTTONDOWN:
			global->click=true;
			break;
		case WM_LBUTTONUP:
			global->click=false;
			break;
		case WM_RBUTTONDOWN:
			global->rclick=true;
			break;
		case WM_RBUTTONUP:
			global->rclick=false;
			break;
		case WM_MOUSEMOVE:
			global->crosshair.x=((LOWORD(lParam)/(float)global->device.right)*(global->rect.right*2.0f))-global->rect.right;
			global->crosshair.y=((HIWORD(lParam)/(float)global->device.bottom)*(global->rect.bottom*2.0f))-global->rect.bottom;
			break;
		case WM_CLOSE:
			if(wParam!=1&&MessageBox(hwnd,"Are you sure you want to quit?\nYour progress will be saved.","Tower Defense",MB_YESNO)==IDNO)break;
			global->running=false;
			while(!global->quit);
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_ERASEBKGND:break;
		default:
			return DefWindowProc(hwnd,msg,wParam,lParam);
			break;
	}
	return 0;
}int wndproc_line=__LINE__;
