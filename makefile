name=TowerDefense
obj=main.o wndproc.o core.o object.o menu.o misc.o resource.res
lflags=libcomgl.a -lopengl32 -mwindows -s
cflags=-std=c99 -O2 -c
$(name).exe:$(obj) libcomgl.a
	gcc $(obj) $(lflags) -o $(name)
	.\$(name)
%.o:%.c defs.h
	gcc $< $(cflags)
resource.res:resource.rc vert.glsl frag.glsl $(name).pack
	windres -i resource.rc -J rc -o resource.res -O coff
$(name).pack:build.txt *.tga
	pack --build
clean:
	del $(obj) $(name).exe $(name).pack