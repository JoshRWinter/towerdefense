#include <windows.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <math.h>
#include "comgl.h"
#include "defs.h"
void retarget(struct enemy *enemy){
	if(enemy->texture!=TID_X&&enemy->texture!=TID_BOSS){
		enemy->xtarget+=randomint(-30,30)/100.0f;
		enemy->ytarget+=randomint(-30,30)/100.0f;
	}
	if(enemy->texture==TID_BOSS&&!enemy->loc){
		enemy->x-=0.35f;
		enemy->y-=0.35f;
	}
	enemy->rottarget=-atan2f((enemy->y/*+(enemy->h/2.0f)*/)-enemy->ytarget,(enemy->x/*+(enemy->w/2.0f)*/)-enemy->xtarget);
	enemy->xup=cosf(enemy->rottarget)/enemy->speed;
	enemy->yup=sinf(enemy->rottarget)/enemy->speed;
}
int level1waypoint(struct enemy *enemy){
	switch(enemy->loc){
		case -1:
			enemy->x=8.0f;
			enemy->y=-2.27f;
			enemy->loc++;
		case 0:
			enemy->xtarget=2.6f;
			enemy->ytarget=-2.36f;
			retarget(enemy);
			break;
		case 1:
			enemy->xtarget=2.83f;
			enemy->ytarget=3.77f;
			retarget(enemy);
			break;
		case 2:
			enemy->xtarget=-3.0f;
			enemy->ytarget=3.39f;
			retarget(enemy);
			break;
		case 3:
			enemy->xtarget=-2.9f;
			enemy->ytarget=-0.55f;
			retarget(enemy);
			break;
		case 4:
			enemy->xtarget=-9.0f;
			enemy->ytarget=-0.25f;
			retarget(enemy);
			break;
		default:
			return false;
	}
	enemy->loc++;
	return true;
}
int level2waypoint(struct enemy *enemy){
	switch(enemy->loc){
		case -1:
			enemy->x=8.0f;
			enemy->y=-2.0f;
			enemy->loc++;
		case 0:
			enemy->xtarget=-4.8f;
			enemy->ytarget=-2.0f;
			retarget(enemy);
			break;
		case 1:
			enemy->xtarget=-4.65f;
			enemy->ytarget=4.8f;
			retarget(enemy);
			break;
		default:
			return false;
	}
	enemy->loc++;
	return true;
}
int level3waypoint(struct enemy *enemy){
	switch(enemy->loc){
		case -1:
			enemy->x=4.75f;
			enemy->y=-4.8f;
			enemy->loc++;
		case 0:
			enemy->xtarget=4.74f;
			enemy->ytarget=1.84f;
			retarget(enemy);
			break;
		case 1:
			enemy->xtarget=-5.35f;
			enemy->ytarget=1.67f;
			retarget(enemy);
			break;
		case 2:
			enemy->xtarget=-5.2f;
			enemy->ytarget=-4.9f;
			retarget(enemy);
			break;
		default:
			return false;
	}
	enemy->loc++;
	return true;
}
int level4waypoint(struct enemy *enemy){
	switch(enemy->loc){
		case -1:
			enemy->x=3.51f;
			enemy->y=-4.8f;
			enemy->loc++;
		case 0:
			enemy->xtarget=3.5f;
			enemy->ytarget=-2.16f;
			break;
		case 1:
			enemy->xtarget=-3.4f;
			enemy->ytarget=-2.16f;
			break;
		case 2:
			enemy->xtarget=-3.15f;
			enemy->ytarget=0.54f;
			break;
		case 3:
			enemy->xtarget=1.61f;
			enemy->ytarget=0.4f;
			break;
		case 4:
			enemy->xtarget=1.61f;
			enemy->ytarget=4.8f;
			break;
		default:
			return false;
	}
	retarget(enemy);
	enemy->loc++;
	return true;
}
int level5waypoint(struct enemy *enemy){
	switch(enemy->loc){
		case -1:
			enemy->x=4.75f;
			enemy->y=-4.7;
			enemy->loc++;
		case 0:
			enemy->xtarget=4.83f;
			enemy->ytarget=0.32f;
			break;
		case 1:
			enemy->xtarget=-4.75f;
			enemy->ytarget=0.23f;
			break;
		case 2:
			enemy->xtarget=-4.42f;
			enemy->ytarget=4.7f;
			break;
		default:
			return false;
	}
	retarget(enemy);
	enemy->loc++;
	return true;
}
int level6waypoint(struct enemy *enemy){
	switch(enemy->loc){
		case -1:
			enemy->x=-8.3f;
			enemy->y=-2.79f;
			enemy->loc++;
		case 0:
			enemy->xtarget=5.38f;
			enemy->ytarget=-2.72f;
			break;
		case 1:
			enemy->xtarget=5.33f;
			enemy->ytarget=0.35f;
			break;
		case 2:
			enemy->xtarget=-1.0f;
			enemy->ytarget=0.32f;
			break;
		case 3:
			enemy->xtarget=-0.79f;
			enemy->ytarget=4.7f;
			break;
		default:
			return false;
	}
	retarget(enemy);
	enemy->loc++;
	return true;
}
void newturret(struct global *global,float x,float y,int id){
	struct turret *turret=malloc(sizeof(struct turret));
	turret->texture=texturetype(id);
	turret->rot=-0.0f;
	turret->x=x;
	turret->y=y;
	turret->type=id;
	turret->reload=0;
	getstats(turret->type,&turret->stats);
	turret->w=TOWER_SIZE;
	turret->h=TOWER_SIZE;
	turret->next=global->turretlist;
	global->turretlist=turret;
}
void newlaser(struct global *global,struct turret *parent){
	struct laser *laser=malloc(sizeof(struct laser));
	laser->texture=TID_LASER;
	laser->w=LASER_SIZE*4;
	laser->h=LASER_SIZE/2;
	laser->x=parent->x+(parent->w/2.0f)-(laser->w/2.0f);
	laser->y=parent->y+(parent->h/2.0f)-(laser->h/2.0f);
	laser->parenttype=parent->type;
	laser->speed=laser->parenttype==LASER_TOWER?6.0f:10.0f;
	laser->target=parent->target;
	laser->rot=-atan2f((laser->y+(LASER_SIZE/2.0))-(laser->target->y+(laser->target->h/2.0)),
	(laser->x+(laser->w/2.0f))-(laser->target->x+(laser->target->w/2.0)));
	laser->xup=-cosf(-laser->rot)/laser->speed;
	laser->yup=-sinf(-laser->rot)/laser->speed;
	laser->kill=false;
	laser->damage=parent->stats.damage;
	laser->next=global->laserlist;
	global->laserlist=laser;
}
void newparticle(struct global *global,float x,float y,int id){
	int num=id==TID_BOSS?22:7;
	for(int i=0;i<num;i++){
		struct particle *particle=malloc(sizeof(struct particle));
		particle->texture=TID_PARTICLE;
		particle->w=PARTICLE_SIZE;
		particle->h=PARTICLE_SIZE;
		particle->x=x-(PARTICLE_SIZE/2.0f);
		particle->y=y-(PARTICLE_SIZE/2.0f);
		particle->rot=randomint(0,360)*(pi/180.0f);
		particle->rotup=randomint(-10,10)/100.0f;
		int speedmod=randomint(10,15);
		particle->xup=-cosf(-particle->rot)/speedmod;
		particle->yup=-sinf(-particle->rot)/speedmod;
		particle->life=randomint(80,100);
		switch(id){
			case TID_ENEMY1:
				particle->rgb[0]=0.8f;
				particle->rgb[1]=0.0f;
				particle->rgb[2]=0.0f;
				break;
			case TID_ENEMY2:
				particle->rgb[0]=0.0f;
				particle->rgb[1]=0.0f;
				particle->rgb[2]=0.8f;
				break;
			case TID_ENEMY3:
				particle->rgb[0]=0.9f;
				particle->rgb[1]=0.9f;
				particle->rgb[2]=0.0f;
				break;
			case TID_ENEMY4:
				particle->rgb[0]=i%2==0?0.8f:0.0f;
				particle->rgb[1]=0.0f;
				particle->rgb[2]=i%2==0?0.0f:0.8f;
				break;
			case TID_BOSS:
				particle->rgb[0]=0.35f;
				particle->rgb[1]=0.35f;
				particle->rgb[2]=0.35f;
				break;
		}
		particle->next=global->particlelist;
		global->particlelist=particle;
	}
}
void newenemy(struct global *global){
	if(global->wave%5==0){
		if(global->enemylist!=NULL)return;
		global->remaining=1;
		struct enemy *enemy=malloc(sizeof(struct enemy));
		enemy->texture=TID_BOSS;
		enemy->w=0.7f;
		enemy->h=0.7f;
		enemy->speed=78;
		enemy->loc=-1;
		global->waypoint(enemy);
		enemy->health=(global->healthadd*HEALTHMULT)*12.0f;
		enemy->rot=enemy->rottarget;
		enemy->next=global->enemylist;
		global->enemylist=enemy;
		return;
	}
	struct enemy *enemy=malloc(sizeof(struct enemy));
	enemy->texture=TID_ENEMY1+rand()%4;
	enemy->w=0.3f;
	enemy->h=0.3f;
	enemy->speed=randomint(50,55);
	enemy->loc=-1;
	global->waypoint(enemy);
	enemy->health=(global->healthadd*HEALTHMULT)+(randomint(-global->wave,global->wave)*2);
	enemy->rot=enemy->rottarget;
	enemy->next=global->enemylist;
	global->enemylist=enemy;
}
void newpath(struct global *global,float x,float y){
	struct generic *path=malloc(sizeof(struct generic));
	path->texture=TID_BUILDMENU;
	path->w=1.2f;
	path->h=1.2f;
	path->x=x-(path->w/2.0f);
	path->y=y-(path->h/2.0f);
	path->rot=0.0f;
	path->next=global->pathlist;
	global->pathlist=path;
}
int collide(void *v1,void *v2){
	struct generic *a=v1,*b=v2;
	return a->x+a->w>b->x&&a->x<b->x+b->w&&a->y+a->h>b->y&&a->y<b->y+b->h;
}
int mouseover(struct global *global,void *v){
	struct generic *a=v;
	return global->crosshair.x>a->x&&global->crosshair.x<a->x+a->w&&global->crosshair.y>a->y&&global->crosshair.y<a->y+a->h;
}
void draw(struct global *global,void *vtarget){
	struct generic *target=vtarget;
	glUniform4fv(global->uniform.texcoords,1,global->assets.texture[target->texture].pinch);
	glUniform2f(global->uniform.vector,target->x,target->y);
	glUniform2f(global->uniform.size,target->w,target->h);
	glUniform1f(global->uniform.rot,target->rot);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
}int object_line=__LINE__;
